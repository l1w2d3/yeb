let proxyObj={}

proxyObj['/']={
    //websocket
    ws:false,
    //目标地址 
    //target: 'https://picsum.photos',
    target: 'https://source.unsplash.com',  
    //请求头的host被设置为target
    changeOrigin:true,
    //不重写请求地址
    pathReWrite:{
        '^/':'/'
    }
}

module.exports={
    devServer:{
        host:'localhost',
        port:8080,
        proxy: proxyObj
    }
}