import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue' 
   
Vue.use(VueRouter)
     
const routes = [
  {
    path: '/',
    name: 'Login1',
    component: Login,
    hidden:true
  }
]

const router = new VueRouter({
  routes//添加其他属性会影响？
})
 
export default router
