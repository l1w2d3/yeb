import Vue from 'vue'
import App from './App.vue'
import router from './router' 
import store from './store' 
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import 'font-awesome/css/font-awesome.css';
//http://nodejs.cn/api/errors.html
//https://zhuanlan.zhihu.com/p/154457244
import { postRequest } from './utils/api' 
import { getRequest} from './utils/api' 
import { putRequest } from './utils/api' 
import { deleteRequest } from './utils/api'  
import { initMenu } from './utils/menus';
import peopleList  from './utils/peopleList'
      
Vue.config.productionTip = false
Vue.use(ElementUI);

Vue.prototype.postRequest = postRequest;
Vue.prototype.getRequest = getRequest;
Vue.prototype.putRequest = putRequest;
Vue.prototype.deleteRequest = deleteRequest;

router.beforeEach((to,from,next)=>{
  
  console.log('from',from);
  console.log('to',to);
  console.log('next11111111111111',next);
  if(window.sessionStorage.getItem('tokenStr')){//是否登录
    initMenu(router,store)//菜单数据
    if(!window.sessionStorage.getItem("user")){
      let resp = peopleList;
      if(resp){
        window.sessionStorage.setItem('user',JSON.stringify(resp)); 
        console.log('next22223333',next);
        next();
      }
    }  
    console.log('next2222222222222',next);
    next();
    //next({ ...to, replace: true })
  }else{
    if(to.path=='/'){//是否去登录页       
      next();
    }else{
      console.log('next3333333333',next);
      next('/?redirect='+to.path);
    }
  } 
})
//从app导出，main.js渲染        
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
