const peopleList = {
    "id":1,
    "name": "系统管理员",
    "phone":"13812361398",
    "address":"香港特别行政区强县长寿柳州路p座123",
    "enabled":"true",
    "username":"admin",
    "password":null,
    "userFace":"https://cube.elemecdn.com/0/88/03b0d39583f48206768a7534e55bcpng.png",
    "remark":null,
    "roles":[
        {
            "id":6,
            "name":"ROLE_admin",
            "nameZh":"系统管理员"
        }
    ],
    "authorities":[
        {
            "authority":"ROLE_admin"
        }
    ],
    "accountNonExpired":true,
    "creadentialsNonExpires":true,
    "accountNonLocked":true
}
export default peopleList