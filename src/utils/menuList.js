 
const menuList = [
  {
    "id": 2,
    "url": "/",
    "path": "/home",
    "component": "Home",
    "name": "员工资料",
    "iconCls": "fa fa-user-circle-o",
    "keepAlive": null,
    "requireAuth": true,
    "parentId": 1,
    "enabled": true,
    "children": [
      {
        "id": 7,
        "url": "/employee/basic/**",
        "path": "/emp/basic",
        "component": "EmpBasic",
        "name": "基本资料",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 2,
        "enabled": true,
        "children": null,
        "roles": null
      },
      {
        "id": 8,
        "url": "/employee/adv/**",
        "path": "/emp/adv",
        "component": "EmpAdv",
        "name": "高级资料",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 2,
        "enabled": true,
        "children": null,
        "roles": null
      }
    ],
    "roles": null
  },
  {
    "id": 3,
    "url": "/",
    "path": "/home",
    "component": "Home",
    "name": "人事管理",
    "iconCls": "fa fa-user-circle-o",
    "keepAlive": null,
    "requireAuth": true,
    "parentId": 1,
    "enabled": true,
    "children": [
      {
        "id": 9,
        "url": "/personel/emp/**",
        "path": "/per/emp",
        "component": "PerEmp",
        "name": "员工资料管理",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 3,
        "enabled": true,
        "children": null,
        "roles": null
      },
      {
        "id": 10,
        "url": "/personel/ec/**",
        "path": "/per/ec",
        "component": "PerEc",
        "name": "员工奖惩",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 3,
        "enabled": true,
        "children": null,
        "roles": null
      },
      {
        "id": 11,
        "url": "/personel/train/**",
        "path": "/per/train",
        "component": "PerTrain",
        "name": "员工培训",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 3,
        "enabled": true,
        "children": null,
        "roles": null
      },
      {
        "id": 12,
        "url": "/personel/salary/**",
        "path": "/per/salary",
        "component": "PerSalary",
        "name": "员工调薪",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 3,
        "enabled": true,
        "children": null,
        "roles": null
      },
      {
        "id": 13,
        "url": "/personel/remove/**",
        "path": "/per/mv",
        "component": "PerMv",
        "name": "员工调动",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 3,
        "enabled": true,
        "children": null,
        "roles": null
      }
    ],
    "roles": null
  },
  {
    "id": 4,
    "url": "/",
    "path": "/home",
    "component": "Home",
    "name": "薪资管理",
    "iconCls": "fa fa-money",
    "keepAlive": null,
    "requireAuth": true,
    "parentId": 1,
    "enabled": true,
    "children": [
      {
        "id": 14,
        "url": "/salary/sob/**",
        "path": "/sal/sob",
        "component": "SalSob",
        "name": "工资账套管理",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 4,
        "enabled": true,
        "children": null,
        "roles": null
      },
      {
        "id": 15,
        "url": "/salary/sobcfg/**",
        "path": "/sal/sobcfg",
        "component": "SalSobCfg",
        "name": "员工账套设置",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 4,
        "enabled": true,
        "children": null,
        "roles": null
      },
      {
        "id": 16,
        "url": "/salary/table/**",
        "path": "/sal/table",
        "component": "SalTable",
        "name": "工资表管理",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 4,
        "enabled": true,
        "children": null,
        "roles": null
      },
      {
        "id": 17,
        "url": "/salary/month/**",
        "path": "/sal/month",
        "component": "SalMonth",
        "name": "月末处理",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 4,
        "enabled": true,
        "children": null,
        "roles": null
      },
      {
        "id": 18,
        "url": "/salary/search/**",
        "path": "/sal/search",
        "component": "SalSearch",
        "name": "工作表单查询",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 4,
        "enabled": true,
        "children": null,
        "roles": null
      },
    ],
    "roles": null
  },
  {
    "id": 5,
    "url": "/",
    "path": "/home",
    "component": "Home",
    "name": "统计管理",
    "iconCls": "fa fa-bar-chart",
    "keepAlive": null,
    "requireAuth": true,
    "parentId": 1,
    "enabled": true,
    "children": [
      {
        "id": 19,
        "url": "/statistics/all/**",
        "path": "/sta/all",
        "component": "StaAll",
        "name": "综合信息统计",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 5,
        "enabled": true,
        "children": null,
        "roles": null
      },
      {
        "id": 20,
        "url": "/statistics/score/**",
        "path": "/sta/score",
        "component": "StaScore",
        "name": "员工积分统计",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 5,
        "enabled": true,
        "children": null,
        "roles": null
      },
      {
        "id": 21,
        "url": "/statistics/personnel/**",
        "path": "/sta/personnel",
        "component": "StaPers",
        "name": "人事信息统计",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 5,
        "enabled": true,
        "children": null,
        "roles": null
      },
      {
        "id": 22,
        "url": "/statistics/recored/**",
        "path": "/sta/recored",
        "component": "StaRecored",
        "name": "人事记录统计",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 5,
        "enabled": true,
        "children": null,
        "roles": null
      },

       
    ],
    "roles": null
  },
  {
    "id": 6,
    "url": "/",
    "path": "/home",
    "component": "Home",
    "name": "系统管理",
    "iconCls": "fa fa-windows",
    "keepAlive": null,
    "requireAuth": true,
    "parentId": 1,
    "enabled": true,
    "children": [
      {
        "id": 23,
        "url": "/systemm/basic/**",
        "path": "/sys/basic",
        "component": "SysBasic",
        "name": "基础信息设置",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 6,
        "enabled": true,
        "children": null,
        "roles": null
      },
      {
        "id": 24,
        "url": "/systemm/copnfig/**",
        "path": "/sys/copnfig",
        "component": "SysCopnfig",
        "name": "系统设置",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 6,
        "enabled": true,
        "children": null,
        "roles": null
      },
      {
        "id": 25,
        "url": "/systemm/log/**",
        "path": "/sys/log",
        "component": "SysLog",
        "name": "操作日志管理",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 6,
        "enabled": true,
        "children": null,
        "roles": null
      },
      {
        "id": 26,
        "url": "/systemm/admin/**",
        "path": "/sys/admin",
        "component": "SysAdmin",
        "name": "操作员管理",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 6,
        "enabled": true,
        "children": null,
        "roles": null
      },
      {
        "id": 27,
        "url": "/systemm/data/**",
        "path": "/sys/data",
        "component": "SysData",
        "name": "备份数据恢复",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 6,
        "enabled": true,
        "children": null,
        "roles": null
      },
      {
        "id": 28,
        "url": "/systemm/init/**",
        "path": "/sys/init",
        "component": "SysInit",
        "name": "初始化数据库",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 6,
        "enabled": true,
        "children": null,
        "roles": null
      },
        
    ],
    "roles": null
  }


]
const menuList2 = [
  {
    "id": 2,
    "url": "/",
    "path": "/home",
    "component": "Home",
    "name": "员工资料",
    "iconCls": "fa fa-user-circle-o",
    "keepAlive": null,
    "requireAuth": true,
    "parentId": 1,
    "enabled": true,
    "children": [
      {
        "id": 7,
        "url": "/employee/basic/**",
        "path": "/emp/basic",
        "component": "EmpBasic",
        "name": "基本资料",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 2,
        "enabled": true,
        "children": null,
        "roles": null
      }, 
    ],
    "roles": null
  },
  {
    "id": 3,
    "url": "/",
    "path": "/home",
    "component": "Home",
    "name": "人事管理",
    "iconCls": "fa fa-user-circle-o",
    "keepAlive": null,
    "requireAuth": true,
    "parentId": 1,
    "enabled": true,
    "children": [
      {
        "id": 9,
        "url": "/personel/emp/**",
        "path": "/per/emp",
        "component": "PerEmp",
        "name": "员工资料管理",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 3,
        "enabled": true,
        "children": null,
        "roles": null
      },
      {
        "id": 10,
        "url": "/personel/ec/**",
        "path": "/per/ec",
        "component": "PerEc",
        "name": "员工奖惩",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 3,
        "enabled": true,
        "children": null,
        "roles": null
      },
      {
        "id": 11,
        "url": "/personel/train/**",
        "path": "/per/train",
        "component": "PerTrain",
        "name": "员工培训",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 3,
        "enabled": true,
        "children": null,
        "roles": null
      },
      {
        "id": 12,
        "url": "/personel/salary/**",
        "path": "/per/salary",
        "component": "PerSalary",
        "name": "员工调薪",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 3,
        "enabled": true,
        "children": null,
        "roles": null
      },
      {
        "id": 13,
        "url": "/personel/remove/**",
        "path": "/per/mv",
        "component": "PerMv",
        "name": "员工调动",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 3,
        "enabled": true,
        "children": null,
        "roles": null
      }
    ],
    "roles": null
  },
  {
    "id": 4,
    "url": "/",
    "path": "/home",
    "component": "Home",
    "name": "薪资管理",
    "iconCls": "fa fa-money",
    "keepAlive": null,
    "requireAuth": true,
    "parentId": 1,
    "enabled": true,
    "children": [
      {
        "id": 14,
        "url": "/salary/sob/**",
        "path": "/sal/sob",
        "component": "SalSob",
        "name": "工资账套管理",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 4,
        "enabled": true,
        "children": null,
        "roles": null
      },
      {
        "id": 15,
        "url": "/salary/sobcfg/**",
        "path": "/sal/sobcfg",
        "component": "SalSobCfg",
        "name": "员工账套设置",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 4,
        "enabled": true,
        "children": null,
        "roles": null
      },
      {
        "id": 16,
        "url": "/salary/table/**",
        "path": "/sal/table",
        "component": "SalTable",
        "name": "工资表管理",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 4,
        "enabled": true,
        "children": null,
        "roles": null
      },
      {
        "id": 17,
        "url": "/salary/month/**",
        "path": "/sal/month",
        "component": "SalMonth",
        "name": "月末处理",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 4,
        "enabled": true,
        "children": null,
        "roles": null
      },
      {
        "id": 18,
        "url": "/salary/search/**",
        "path": "/sal/search",
        "component": "SalSearch",
        "name": "工作表单查询",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 4,
        "enabled": true,
        "children": null,
        "roles": null
      },
    ],
    "roles": null
  },
  {
    "id": 5,
    "url": "/",
    "path": "/home",
    "component": "Home",
    "name": "统计管理",
    "iconCls": "fa fa-bar-chart",
    "keepAlive": null,
    "requireAuth": true,
    "parentId": 1,
    "enabled": true,
    "children": [
      {
        "id": 19,
        "url": "/statistics/all/**",
        "path": "/sta/all",
        "component": "StaAll",
        "name": "综合信息统计",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 5,
        "enabled": true,
        "children": null,
        "roles": null
      },
      {
        "id": 20,
        "url": "/statistics/score/**",
        "path": "/sta/score",
        "component": "StaScore",
        "name": "员工积分统计",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 5,
        "enabled": true,
        "children": null,
        "roles": null
      },
      {
        "id": 21,
        "url": "/statistics/personnel/**",
        "path": "/sta/personnel",
        "component": "StaPers",
        "name": "人事信息统计",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 5,
        "enabled": true,
        "children": null,
        "roles": null
      },
      {
        "id": 22,
        "url": "/statistics/recored/**",
        "path": "/sta/recored",
        "component": "StaRecored",
        "name": "人事记录统计",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 5,
        "enabled": true,
        "children": null,
        "roles": null
      },

       
    ],
    "roles": null
  }, 
  {
    "id": 6,
    "url": "/",
    "path": "/home",
    "component": "Home",
    "name": "系统管理权限菜单2",
    "iconCls": "fa fa-windows",
    "keepAlive": null,
    "requireAuth": true,
    "parentId": 1,
    "enabled": true,
    "children": [
      {
        "id": 23,
        "url": "/systemm/basic/**",
        "path": "/sys/basic",
        "component": "SysBasic",
        "name": "基础信息设置",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 6,
        "enabled": true,
        "children": null,
        "roles": null
      },
      {
        "id": 24,
        "url": "/systemm/copnfig/**",
        "path": "/sys/copnfig",
        "component": "SysCopnfig",
        "name": "系统设置",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 6,
        "enabled": true,
        "children": null,
        "roles": null
      },
      {
        "id": 25,
        "url": "/systemm/log/**",
        "path": "/sys/log",
        "component": "SysLog",
        "name": "操作日志管理",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 6,
        "enabled": true,
        "children": null,
        "roles": null
      },
      {
        "id": 26,
        "url": "/systemm/admin/**",
        "path": "/sys/admin",
        "component": "SysAdmin",
        "name": "操作员管理",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 6,
        "enabled": true,
        "children": null,
        "roles": null
      },
      {
        "id": 27,
        "url": "/systemm/data/**",
        "path": "/sys/data",
        "component": "SysData",
        "name": "备份数据恢复",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 6,
        "enabled": true,
        "children": null,
        "roles": null
      },
      {
        "id": 28,
        "url": "/systemm/init/**",
        "path": "/sys/init",
        "component": "SysInit",
        "name": "初始化数据库",
        "iconCls": null,
        "keepAlive": null,
        "requireAuth": true,
        "parentId": 6,
        "enabled": true,
        "children": null,
        "roles": null
      },
        
    ],
    "roles": null
  }
   
]
export default menuList
export { menuList2 }
//https://www.cnblogs.com/-nothing-/p/9152173.html