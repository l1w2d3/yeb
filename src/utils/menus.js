import { getRequest } from "./api"
import menuList from './menuList'
import {menuList2} from './menuList'
export const initMenu = (router, store) => {
    if (store.state.routes.length > 0 ) {
        return;
    } 
    // getRequest('/system/config.menu').then(data=>{
    //     if(data){
    //         let fmroutes= formatRoutes(data);  
    //         store.commit('initRoutes',fmroutes)
    //     } 
    //     let data2 = Math.random()
    //     //仿后台给不同的菜单
    //     if(data2>0.5){  
    //         data2=menuList; 
    //     }else{ 
    //         data2=menuList2; 
    //     }  
    //     let fmroutes = formatRoutes(data2);
    //     router.addRoutes(fmroutes); //npm i vue-router@3.0 -S
    //     // for (let x of fmroutes) {
    //     //     router.addRoute(x)
    //     // } //npm i vue-router@4.0 -S  
    //     store.commit('initRoutes', fmroutes)
    // }) 
     
        let data2 = Math.random()
        if(data2>0.5){  
            data2=menuList; 
            console.log('addRoute111111');
        }else{ 
            data2=menuList2; 
            console.log('addRoute111111');
        }  
        let fmroutes = formatRoutes(data2);//把对象格式化为route可以用和引入的
        router.addRoutes(fmroutes);  //添加到router 
       
        store.commit('initRoutes', fmroutes)//储存在本地 
    
}
export const formatRoutes = (routes) => {
    let fmtRoutes = [];
    routes.forEach(router => {
        let {
            path,
            component,
            name,
            iconCls,
            children,
        } = router;
        if (children && children instanceof Array) {
            children = formatRoutes(children)
        }
        let fmRouter = {
            path: path,
            name: name,
            iconCls: iconCls,
            children:children,
            component(resolve) { 
                if(component.startsWith('Home')){
                    require(['../views/'+ component+'.vue'],resolve);
                }        
                else if(component.startsWith('Emp')) {
                    require(['../views/emp/'+ component+'.vue'],resolve);
                }else if(component.startsWith('Per')) {
                    require(['../views/per/'+ component+'.vue'],resolve);
                }else if(component.startsWith('Sal')) {
                    require(['../views/sal/'+ component+'.vue'],resolve);
                }else if(component.startsWith('Sta')) {
                    require(['../views/sta/'+ component+'.vue'],resolve);
                }else if(component.startsWith('Sys')) {
                    require(['../views/sys/'+ component+'.vue'],resolve);
                } 
            }
            //component: () => import('../view/'+component+'.vue')
        }
        fmtRoutes.push(fmRouter)
    });
    return fmtRoutes;
}